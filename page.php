<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package socius_custom
 */

$product_category = get_field( 'service_type' );

if($product_category == "general") {
    $product = 'windows';
}   elseif($product_category == "about") {
    $product = 'about';
}   elseif($product_category == "windows") {
    $product = 'windows';
}   elseif($product_category == "doors") {
    $product = 'doors';
}   elseif($product_category == "bath") {
    $product = 'bath';
}   elseif($product_category == "pellet-stoves") {
    $product = 'pellet-stoves';
}   else {
    $product = 'windows';
}


get_header(); ?>

	<div id="primary" class="content-area default-area">
		<main id="main" class="site-main default-main">
            <?php if( get_field('sidebar_form') !== 'Hide' ): ?>
                <div class="custom-sidebar">
                    <?php if($product == 'windows'): ;?>
                        <?php wp_nav_menu( array( 'theme_location' => 'windows_menu', 'menu_class' => 'sidebar-menu' ) ); ?>
                    <?php elseif ($product == 'about'): ;?>
                        <?php wp_nav_menu( array( 'theme_location' => 'about_menu', 'menu_class' => 'sidebar-menu' ) ); ?>
                    <?php elseif ($product == 'bath'): ;?>
                        <?php wp_nav_menu( array( 'theme_location' => 'bathrooms_menu', 'menu_class' => 'sidebar-menu' ) ); ?>
                    <?php endif;?>
                </div>
            <?php endif;?>
            <div class="main-content-area" >
			<?php
			while ( have_posts() ) : the_post();
                get_template_part( 'template-parts/content', 'page' );

                if ( get_field('use_tab_system') == 'Yes' ) {
                    get_template_part( 'template-parts/tab-system' );
                }else {

                }

				// If comments are open or we have at least one comment, load up the comment template.
				/*if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif; */

			endwhile; // End of the loop.
			?>
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_template_part('template-parts/gallery');?>
<?php get_template_part('template-parts/section-offers');?>
<?php get_template_part('template-parts/sweepstakes');?>

<?php
//get_sidebar();
get_footer();
