<?php
/**
 * Template Name: Homepage
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Compozit
 */

get_header(); ?>

    <?php get_template_part('template-parts/content','homepage'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				/*if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif; */

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

    <?php get_template_part('template-parts/section-product-selector');?>
    <?php get_template_part('template-parts/sweepstakes');?>
    <?php get_template_part('template-parts/why-us');?>
    <?php get_template_part('template-parts/section-offers');?>
<?php
//get_sidebar();
get_footer();
