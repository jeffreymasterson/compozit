<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package socius_custom
 */
//Paths
$theme_dir = get_bloginfo('template_directory');
?><!DOCTYPE html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" href="<?php bloginfo('url'); ?>/favicon.ico" type="image/x-icon">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'socius_custom' ); ?></a>
	<!--[if lte IE 9]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <?php if(is_front_page()): ?>
	<div class="masthead">
    <?php else: ?>
    <div class="internal-masthead">
        <?php endif; ?>
	<?php get_template_part( 'template-parts/content', 'block-nav' ); ?>
    <?php if(is_front_page()): ?>
        <?php get_template_part('template-parts/home-hero'); ?>
    <?php else: ?>
        <?php get_template_part( 'template-parts/internal-hero' ); ?>
    <?php endif; ?>
    </div>

	<div id="content" class="site-content d-block w-100">