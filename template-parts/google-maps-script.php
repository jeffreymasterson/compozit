<?php
$map_settings = get_field('map_settings', 'option');
$api_key = isset($map_settings['google_api_key']) && !empty($map_settings['google_api_key']) ? $map_settings['google_api_key'] : '';
$map_zoom_level = isset($map_settings['map_zoom_level']) ? $map_settings['map_zoom_level'] : '12';
$map_marker_icon = isset($map_settings['map_marker_icon']['url']) && !empty($map_settings['map_marker_icon']['url']) ? $map_settings['map_marker_icon']['url'] : '';
$map_styles = isset($map_settings['map_styles']) && !empty($map_settings['map_styles']) ? $map_settings['map_styles'] : ' [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#EBE5E0"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]}]';
?>

<script src="//maps.googleapis.com/maps/api/js?key=<?php echo $api_key; ?>" defer="defer"></script>

<script>
var maps = {}, map, $markers;

(function($) {

	/*
	*  This function will render a Google Map onto the selected jQuery element
	*/

	function new_map( $el, $specificZoom ) {
	    // var
	    $markers = $el.find('.marker');
        if($el.hasClass('footer-map')){
            var mainMap = true;
        }else{
            var mainMap = false;
        }
        
        if($specificZoom == ''){
            $specificZoom = <?php echo $map_zoom_level; ?>;
        }
        
        var args = {
            zoom        : $specificZoom,
            center      : new google.maps.LatLng(0, 0),
            mapTypeControl: true,
            panControl: true,
            scrollwheel: false,
            draggable: true,
            disableDefaultUI: true,
            styles: <?php echo $map_styles; ?>
        };
        
	    // vars
        if($el.hasClass('projects')){
    	    if($(window).width() < 1023){
                var args = {
                    zoom        : 7,
        	        center      : new google.maps.LatLng(0, 0),
        	        mapTypeControl: true,
        	        panControl: true,
        	        scrollwheel: false,
        	        draggable: true,
        	        disableDefaultUI: true,
        	        styles: <?php echo $map_styles; ?>
        	    };
            }
        }
        
	    // create map
	    var map = new google.maps.Map( $el[0], args);
        
	    // add a markers reference
	    map.markers = [];

	    // add markers
	    $markers.each(function(){
	        add_marker( $(this), map, $el );
	    });

	    // center map
	    center_map( map, mainMap );

	    // return
	    return map;
	}

	/*
	*  This function will add a marker to the selected Google Map
	*/
	var prev_infowindow =false;
	function add_marker( $marker, map, $el ) {

	    // var
	    var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	    var activeMarker = '';
        var $customMarkerAttr = $marker.attr('data-marker');
        var $customMarker = $customMarkerAttr;
        var $customActiveMarkerAttr = $marker.attr('data-marker-active');
        var $customActiveMarker = $customActiveMarkerAttr;
        
        if( $el.hasClass('projects') ) {
            if($customMarkerAttr.length > 0){
                var $customMarker = $customMarkerAttr;
                var $customActiveMarker = $customActiveMarkerAttr;
            }else{
                var $customMarker = "<?php echo get_template_directory_uri() . '/images/icon-project-placeholder.png'; ?>";
                var $customActiveMarker = "<?php echo get_template_directory_uri() . '/images/icon-project-placeholder-blue.png'; ?>";
            }
        }
	    // create marker
	    var marker = new google.maps.Marker({
	        record_id: $marker.data('id'),
	        position    : latlng,
	        map         : map,
	        icon: $customMarker,
            icon_active	: $customActiveMarker,
	    });

	    // add to array
	    map.markers.push( marker );

        if( $el.hasClass('projects') && marker.record_id == 1 ) {
            changeMarker(map, marker.record_id);
        }else{
    	    // if marker contains HTML, add it to an infoWindow PLEASE UNCOMMET TO ADD INFO WINDOW BACK IN
    	    if( $marker.html() ){
    	        // create info window
    	        var infowindow = new google.maps.InfoWindow({
    	            content     : $marker.html()
    	        });

    	        // show info window when marker is clicked
    	        google.maps.event.addListener(marker, 'click', function() {
    				if( prev_infowindow ) {
    					prev_infowindow.close();
    				}

    				prev_infowindow = infowindow;
    				infowindow.open( map, marker );
    	        });
    	    }
        }
        
        
        
        google.maps.event.addListener(marker, 'click', function() {
			if($el.hasClass('projects')) {
				$('.project-map-container .tab-content').find('.active').removeClass('active show');
				$('.project-map-container .tab-content [data-project-id="' + marker.record_id + '"]').addClass('active show');
                map_size();
                changeMarker(map, marker.record_id);
			}
        });
        
	}

	/*
	*  This function will center the map, showing all markers attached to this map
	*/

	function center_map( map, mainMap ) {

	    // vars
	    var bounds = new google.maps.LatLngBounds();

	    // loop through all markers and create bounds
	    $.each( map.markers, function( i, marker ){
	        var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
	        bounds.extend( latlng );
	        google.maps.event.addListener(marker, 'click', function(){
	        	var clicked_marker_id = i;
	        	$.each( map.markers, function( i, marker ){
	        		if(clicked_marker_id == i){
	        		}else{
	        			// marker.setIcon('<?php echo $map_marker_icon; ?>');
	        		}
	        	});
	        	$('.map-box a.active').removeClass('active');
	        	var marker_id = marker.record_id;
	        	$('.map-box a#'+ marker_id).addClass('active');
	        	// marker.setIcon('<?php echo $map_marker_icon; ?>');
			// marker.setAnimation(google.maps.Animation.BOUNCE);
	        });
	    });
        
        if(mainMap == true){
            // map.fitBounds( bounds );
            map.setCenter( bounds.getCenter() );
                map.panBy(0, 0);
        }else{
            // only 1 marker?
    	    if( map.markers.length == 1 ){
    	        // set center of map
    	        map.setCenter( bounds.getCenter() );
    	    }else{
    	        // fit to bounds
    	        // map.fitBounds( bounds );
                map.setCenter( bounds.getCenter() );
    	    }
        }
	}

	/*
	*  This function will render each map when the document is ready (page has loaded)
	*/    
    $(document).ready(function(){
		$('.google-map').each(function(){
			var $container = $(this),
				id = $container.attr('id');
            var specificZoom = JSON.stringify($(this).data('zoom'));
            if (typeof specificZoom == typeof undefined || specificZoom == false) {
                specificZoom = '';
            }else{
                var specificZoom = JSON.parse(specificZoom);
            }
            
			// create map
			maps[id] = {map: new_map($container, specificZoom)};
		});
	});

})(jQuery);

var prev_marker = false;
var prev_icon = false;
function changeMarker(map, record_id){
    if( prev_marker ) {
        for (i in map.markers){
            if(map.markers[i].record_id == prev_marker){
                map.markers[i].setIcon(prev_icon);
            }
        }
    }
    
    prev_marker = record_id;
    for (i in map.markers){
        if(map.markers[i].record_id == record_id){
            prev_icon = map.markers[i].icon;
            marker = map.markers[i];
        }
    }
    
    marker.setIcon(marker.icon_active);
}

var firstTabHeight = $('.project-map-container .tab-pane:first-child').outerHeight();
var differenceFromFullHeight = $('.project-map-container .map-content').outerHeight() - firstTabHeight;
function map_size() {
    var vpWidth = viewport().width; // This should match media query
    var activeTabHeight = $('.project-map-container .tab-pane.active').outerHeight();
    if (vpWidth < 992) {
        $('.project-map-container .map-content').css({'height' : 'calc(' + activeTabHeight + 'px + ' + differenceFromFullHeight + 'px)'});
    } else {
        $('.project-map-container .map-content').css({'height' : 'calc(' + activeTabHeight + 'px + ' + differenceFromFullHeight + 'px)'});
    }
}

</script>