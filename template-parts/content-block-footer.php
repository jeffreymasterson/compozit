<?php

//Paths
$theme_dir = get_bloginfo('template_directory');

$footer_logo = get_field( 'footer_logo', 'option' );
$footer_text = get_field( 'about_text', 'option' );


?>
<footer class="footer-wrapper">
    <div class="flex-left-footer">
        <div class="footer-logo lazyload" data-bg="<?php echo $footer_logo; ?>"></div>
        <div class="footer-left-text"><?php echo $footer_text; ?></div>
    </div>
    <div class="footer-map">
        <div class="map footer-widget-map" style="overflow: hidden;">
            <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                <?php get_template_part( 'template-parts/google-map' ); ?>
            </div>
        </div>
    </div>
    <div class="flex-right-footer">
        <div class="footer-menu-col-one">
            <div class="footer-menu-titles">
                <div class="footer-menu-titles">Products</div>
            </div>
            <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => '' ) ); ?>
        </div>
        <div class="footer-col-two">
            <div class="footer-menu-titles">
                <div class="footer-menu-titles">Company</div>
            </div>
            <?php wp_nav_menu( array( 'theme_location' => 'footer_two', 'menu_class' => '' ) ); ?>
        </div>
    </div>
    <div class="footer-background lazyload" data-bg="<?php echo $theme_dir; ?>/images/img-content-mask2.png"></div>
</footer>