<div class="google-map" id="footer-map">
     
     <?php
     $locations = get_field('locations_repeater', 'option');
     $map_settings = get_field('map_settings', 'option');
     $default_marker = isset($map_settings['map_marker_icon']['url']) ? $map_settings['map_marker_icon']['url'] : get_template_directory_uri() . '/images/icon-placeholder.png';
     $marker_placeholder = get_template_directory_uri() . '/images/icon-placeholder.png';
     if($locations){
          $i  = 1;
          foreach($locations as $location){
               $location_link = isset($location['location_link']) ? $location['location_link'] : "";
               $location_name = isset($location['location_name']) ? $location['location_name'] : "";
               $lat = isset($location['latitude']) ? $location['latitude'] : "";
               $lng = isset($location['longitude']) ? $location['longitude'] : "";
               $address = isset($location['address']) ? $location['address'] : "";
               $city = isset($location['city']) ? $location['city'] : "";
               $state = isset($location['state']) ? $location['state'] : "";
               $zip = isset($location['zip']) ? $location['zip'] : "";
               $map_marker = isset($location['map_marker_icon']) ? $location['map_marker_icon']['url'] : "";
               if(empty($map_marker)){
                    $map_marker = $marker_placeholder;
               }
               $location_image = isset($location['location_image']['sizes']['medium']) ? $location['location_image']['sizes']['medium'] : '';
               ?>
     
               <div class="marker" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>" data-id="<?php echo $i; ?>" data-marker="<?php echo $map_marker; ?>">
                    <div class="balloon">
                        <div class="location-name"><?php echo $location_name; ?> <span></span></div>
                         <div class="location-address"><?php echo $address; ?><br/><?php echo $city; ?>, <?php echo $state; ?> <?php echo $zip; ?></div>
                         <div class="directions-link"><a target="_blank" href="<?php echo $location_link; ?>">Get Directions</a> </div>
                        <br>
                         <div class="phone-number">(877) 423-0124</div>
                         <img class="custom-icon" src="<?php echo $default_marker; ?>" alt="google maps icon" />
                    </div>
               </div>
     
               <?php
          }
          $i++;
     }

     ?>
         
</div><!-- .google-map -->

<?php get_template_part( 'template-parts/google-maps-script' ); ?>