<?php

//Paths
$theme_dir = get_bloginfo('template_directory');


$home_header = get_field( 'home_content_header_text', 'option');
$home_subheader = get_field( 'home_content_sub_header_text', 'option');
$home_text = get_field( 'home_content_text', 'option');
$home_image = get_field( 'home_content_image', 'option');
$home_btn_text = get_field( 'home_content_button_text', 'option');
$home_btn_link = get_field( 'home_content_button_link', 'option');

?>
<div>
    <a href="<?php echo get_page_link(159); ?>" class="btn btn-default cta-internal-btn">Get a Free Estimate Today</a>
</div>
<div class="homepage-content">
    <div class="content-wrapper">
        <h1 class="b-h1-titles"><?php echo $home_header;?><br></h1>
        <div class="content-subtitle is-about-windows"><?php echo $home_subheader;?></div>
        <p class="content-text"><?php echo $home_text;?><br></p>
        <a href="<?php echo $home_btn_link;?>" class="btn btn-primary content-btn"><?php echo $home_btn_text;?></a>
    </div>
    <div class="content-graphic lazyload" data-bg="<?php echo $home_image;?>">
        <div class="b-content-img-mask lazyload" data-bg="<?php echo $theme_dir; ?>/images/img-content-mask_1.png"></div>
    </div>
</div>