<p><img class="img-responsive" style="padding-right: 12px;" src="http://sociusinc.com/compozithome40228/wp-content/uploads/2018/09/model1090.jpg" alt="" /></p>
<ul class="list-group-item-text">
    <li>Compozit : Frame and sash fusion welded</li>
    <li>Clear double pane IG</li>
    <li>Triple weatherstripping</li>
    <li>3 1/4" jamb depth, sloped sill</li>
    <li>2 locks and keepers standard</li>
    <li>Full length interlock</li>
    <li>Block and tackle balances</li>
    <li>Both sashes tilt in for easy cleaning</li>
    <li>Half fiberglass screen</li>
</ul>
&nbsp;
<h4>Possible Mulled Patterns</h4>
<div class="style-windows-flex">
    <div class="window-choice">
        <img src="http://sociusinc.com/compozithome40228/wp-content/uploads/2018/09/double-hung.gif" alt="" />
        <h6>Double Hung</h6>
    </div>
    <div class="window-choice">
        <img src="http://sociusinc.com/compozithome40228/wp-content/uploads/2018/09/double-double-hung.gif" alt="" />
        <h6>Double Double Hung</h6>
    </div>
    <div class="window-choice">
        <img src="http://sociusinc.com/compozithome40228/wp-content/uploads/2018/09/double-doube-hung-w-pic-window.gif" alt="" />
        <h6>Double Double Hung with Picture Window</h6>
    </div>
    <div class="window-choice">
        <img src="http://sociusinc.com/compozithome40228/wp-content/uploads/2018/09/double-pic-window-double-hung.gif" alt="" />
        <h6>Double Picture Window with Double Hung</h6>
    </div>
    <div class="window-choice">
        <img src="http://sociusinc.com/compozithome40228/wp-content/uploads/2018/09/triple-double-hung.gif" alt="" />
        <h6>Triple Double Hung</h6>
    </div>
</div>

&nbsp;