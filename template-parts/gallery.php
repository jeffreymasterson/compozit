<?php if( get_field('hide_gallery') !== 'Hide' ):
//Paths
    $theme_dir = get_bloginfo('template_directory');

    $product_category = get_field( 'service_type' );

    if($product_category == "general") {
        $product = 'windows';
    }   elseif($product_category == "windows") {
        $product = 'windows';
    }   elseif($product_category == "doors") {
        $product = 'doors';
    }   elseif($product_category == "bath") {
        $product = 'bath';
    }   elseif($product_category == "pellet-stoves") {
        $product = 'pellet-stoves';
    }   else {
        $product = 'windows';
    }
    ?>
    <div class="gallery-section">
        <div class="gallery-section-wrapper">
            <div class="gallery-container">
                <div class="gallery-flex-row">
<!--                    <div class="gallery-col gallery-title-col">-->
<!--                        <div class="gallery-title-wrapper">-->
<!--                            <h2 class="gallery-section-title">Project Gallery</h2>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="gallery-col gallery-filter-col">
                        <ul class="gallery-filter-list nav nav-tabs">
                            <?php if(get_field('product_galleries','option')): $i = 0; ?>
                                <?php while ( have_rows('product_galleries', 'option') ) : the_row();
                                    $title = get_sub_field('gallery_product_name', 'option');
                                    $uniquename = get_sub_field('unique_product_name', 'option');
                                    $i++;
                                    ?>
                                    <li class="gallery-filter-list-item">
                                        <a data-toggle="tab" href="#gallery-<?php echo $i; ?>" class="gallery-filter-list-item-link <?php if( $product === $uniquename ){ echo "active"; } ?>"><?php echo $title; ?></a>
                                        <div class="gallery-filter-list-item-divider">|</div>
                                    </li>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <?php if(get_field('product_galleries','option')): $i = 0; ?>
                    <?php while ( have_rows('product_galleries', 'option') ) : the_row();
                        $title = get_sub_field('gallery_product_name', 'option');
                        $uniquename = get_sub_field('unique_product_name', 'option');
                        $images = get_sub_field('gallery', 'option');
                        $count=0;
                        $i++;
                        ?>
                        <div id="gallery-<?php echo $i; ?>" class="gallery-images-flex-row tab-pane <?php if( $product === $uniquename ){ echo "active"; } ?>">
                            <div class="gallery-section-flex-col">
                                <?php
                                foreach( $images as $image ): ?>
                                <?php if ( in_array($count, range(0, 3)) ): ?>
                                    <div class="gallery-image-col <?php
                                    if ( in_array($count, array(1,2)) ) {
                                        echo 'gallery-image-col-wide';
                                    }
                                    ?>">
                                        <a class="gallery-lightbox-link <?php
                                        if ( in_array($count, array(2,3)) ) {
                                            echo 'gallery-lightbox-link-short';
                                        }
                                        ?> lazyload fancybox" href="<?php echo $image['url']; ?>" data-fancybox="gallery-<?php echo $uniquename; ?>" data-bg="<?php echo $theme_dir; ?>/images/search_wht.svg">
                                            <div class="gallery-lightbox-image lazyload" data-bg="<?php echo $image['url']; ?>"></div>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php // endforeach; ?>
                                <?php if($count == 4): ?>
                            </div>
                            <div class="gallery-section-flex-col">
                                <?php endif; ?>
                                <?php
                                // foreach( $images as $image ): ?>
                                <?php if ( in_array($count, range(4, 7)) ): ?>
                                    <div class="gallery-image-col <?php
                                    if ( in_array($count, array(5,6)) ) {
                                        echo 'gallery-image-col-wide';
                                    }
                                    ?>">
                                        <a class="gallery-lightbox-link <?php
                                        if ( in_array($count, array(4,5)) ) {
                                            echo 'gallery-lightbox-link-short';
                                        }
                                        ?> lazyload fancybox" href="<?php echo $image['url']; ?>" data-fancybox="gallery-<?php echo $uniquename; ?>" data-bg="<?php echo $theme_dir; ?>/images/search_wht.svg">
                                            <div class="gallery-lightbox-image lazyload" data-bg="<?php echo $image['url']; ?>"></div>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php if ( in_array($count, range(8, 100)) ): ?>
                                    <div style="display: none;">
                                        <a class="fancybox" data-fancybox="gallery-<?php echo $uniquename; ?>" href="<?php echo $image['url']; ?>"></a>
                                    </div>
                                    <?php $count++;?>
                                <?php endif; ?>
                                <?php $count++;?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
