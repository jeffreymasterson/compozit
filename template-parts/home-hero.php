<?php

//Paths
$theme_dir = get_bloginfo('template_directory');

$title_text = get_field( 'main_title_text', 'option');
$sub_text = get_field( 'main_sub_title_text', 'option');
$main_text = get_field( 'main_text', 'option');

?>

<div class="hero-area-home" style="background-image: url(<?php echo $theme_dir; ?>/images/roof-cut-off.png);"></div>
<div class="hero-area-boy " style="background-image: url(<?php echo $theme_dir; ?>/images/boy.png);"></div>
<div class="hero-area-components w-clearfix">
    <div class="hero-text-titles">
        <h1 class="hero-area-title"><?php echo $title_text;?><br></h1>
        <div class="b-hero-subtitles"><?php echo $sub_text;?><?php echo $main_text;?><br></div>
        <div class="bouncing-image animated-arrow bounce">
            <img src="<?php echo $theme_dir; ?>/images/arrow.png" alt="arrow icon" width="33">
            <div>Scroll Down</div>
        </div>
    </div>
    <div class="main-hero-form">
        <form id="form4959" name="form4959" action="https://sociusmarketing.wufoo.com/forms/rje3nou1nk5z5k/#public" method="post" novalidate=""  class="form-is-home bvalidate-form">
            <div class="internal-form-title"><strong class="bold-text-form">It's Time To Enhance Your Living</strong></div>
            <div class="b_separator is-short"></div>
            <div class="b_separator"></div>
            <div class="b_separator is-short"></div>
            <div class="main-form-subtitle">Ask For a Totally Free Estimate Today!</div>
            <div class="hero-form-input">
                <input class="internal-form-input" name="Field1" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Your Name:" data-bvalidator="required">
            </div>
            <div class="hero-form-input">
                <input class="internal-form-input" name="Field2" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email address:" required="" data-bvalidator="email,required">
            </div>
            <div class="phone-zip-wrap">
                <div class="hero-form-input zip-field">
                    <input class="internal-form-input" name="Field3" type="tel" value="" maxlength="255" onkeyup="" placeholder="ZIP code">
                </div>
                <div class="hero-form-input">
                    <input class="internal-form-input" name="Field4" data-mask="(000) 000-0000" type="tel" value="" maxlength="10" onkeyup="" placeholder="Phone:" data-bvalidator="minlength[10],required">
                </div>
            </div>
            <div class="submit">
                <input type="submit" value="Get Your Free Quote" data-wait="Please wait..." class="form-btn">
            </div>
            <div style="list-style: none">
                <input type="hidden" name="idstamp" value="j1tNiT8RBWjXzwPesc+AtOwjnEJDpDuUjBqCHfcJSFM=">
            </div>
            <div class="trust-logos">
                <div class="b-left-flex">
                    <img src="<?php echo $theme_dir; ?>/images/5b29755d0f21cf4ad4b6ce86_energystar.png" alt="energy star logo" width="120">
                </div>
                <div class="b-flex-right">
                    <img src="<?php echo $theme_dir; ?>/images/5b297a5802c5c63d79c30168_bbb-logo.png" alt="bbb logo"  width="120" class="image">
                </div>
            </div>
        </form>
    </div>
</div>
<div class="bottom-triangle" style="background-image: url(<?php echo $theme_dir; ?>/images/triangle-bottom.png);"></div>
<div class="top-triangle" style="background-image: url(<?php echo $theme_dir; ?>/images/triangle-top.png);"></div>

<script>
    $(document).ready(function(){
        setTimeout(function(){
            $(".hero-area-home").addClass("home-area-transform");
            $(".top-triangle").addClass("top-triangle-move");
            $(".bottom-triangle").addClass("bottom-triangle-move");
            $(".main-hero-form").addClass("main-hero-move");
        },600)
    });
</script>