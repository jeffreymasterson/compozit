<?php

$theme_dir = get_bloginfo('template_directory');

?>

<div>
    <div class="blog-single-wrapper">
        <a href="<?php the_permalink(); ?>" class="blog-post-item-image-link">
            <?php if (has_post_thumbnail()):?>
                <img class="blog-post-item-image lazyload" data-src="<?php the_post_thumbnail_url('blog-image'); ?>" src="<?php echo $theme_dir; ?>/images/dummy.png" alt="Compozit blog image">
                <div class="flex-col-image-overlay"></div>
            <?php else: ?>
                <img class="blog-post-item-image lazyload" data-src="<?php echo $theme_dir; ?>/images/compozit-blog.jpg" src="<?php echo $theme_dir; ?>/images/dummy.png" alt="Compozit blog image">
                <div class="flex-col-image-overlay"></div>
            <?php endif; ?>
        </a>
        <div class="blog-post-item-text-wrap">
            <h2 class="blog-post-item-title">
                <a href="<?php the_permalink(); ?>" class="blog-post-title-link"><?php echo get_the_title(); ?></a>
            </h2>
            <div class="blog-post-item-data-wrap">
                <div class="blog-post-item-date">Posted on <strong><?php the_time('M'); ?> <?php the_time('d');?>, <?php the_time('Y');?></strong></div>
                <div class="blog-post-item-author">Posted by
                    <strong><a href="<?php the_permalink(); ?>" class="blog-post-author-link"><?php echo $author; ?></a></strong>
                </div>
            </div>
            <p class="blog-post-item-description">
                <?php

                $content = get_the_content();

                $trimmed_content = wp_trim_words( $content, 60 );

                echo $trimmed_content;

                ?>
            </p>
            <a href="<?php the_permalink(); ?>" class="btn btn-default">Read Full Article</a>
        </div>
    </div>
    <div class="divider"></div>
</div>
<?php wp_link_pages( array(

    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'socius_custom' ),

    'after'  => '</div>',

) ); ?>


