<?php

//Paths
$theme_dir = get_bloginfo('template_directory');

$images = get_field('gallery_images', 'option') ? get_field('gallery_images', 'option') : '';
$size = 'large'; // (thumbnail, medium, large, full or custom size)
?>
<div id="gallery-<?php echo $i; ?>" class="gallery-images-flex-row">
    <div class="gallery-section-flex-col">
        <?php
        $images = get_field( 'gallery_images');
        foreach( $images as $image ): ?>
        <div class="gallery-image-col">
            <a class="gallery-lightbox-link lazyload fancybox" href="<?php echo $image['sizes']['large']; ?>" data-fancybox="gallery" data-bg="<?php echo $theme_dir; ?>/images/search_wht.svg">
                <div class="gallery-lightbox-image lazyload" data-bg="<?php echo $image['sizes']['large']; ?>"></div>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
</div>
