<?php
//Paths
$theme_dir = get_bloginfo('template_directory');

$banner_image_hero = get_field( 'hero_image' );

$banner_image_array = get_field('header_images');
if( $banner_image_array ) {
    shuffle($banner_image_array);
    $banner_image = $banner_image_array[0]['url'];
} elseif($banner_image_hero) {
    $banner_image = $banner_image_hero;
} elseif (has_post_thumbnail()) {
    $banner_image = get_the_post_thumbnail_url();
} else {
    $banner_image = get_bloginfo('stylesheet_directory') . '/images/headers/header-'.rand(1,1).'.jpg';
}
?>
<div class="internal-hero w-clearfix" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="internal-hero-mask">
        <h2><?php the_title(); ?></h2>
        <h2></h2>
    </div>
    <?php if(is_page_template('template/sidebar-page.php') || is_page_template('template/contact-page.php') || is_page_template('template/no-form-page.php')): ?>
    <?php else: ?>
    <?php if( get_field('hide_form') !== 'Hide' ): ?>
        <div class="internal-form-area">
            <form id="form4960" name="form4960" action="https://sociusmarketing.wufoo.com/forms/r1iij7771wyew4q/#public" method="post" novalidate="" class="form-is-internal bvalidate-form">
                <div class="internal-form-title"><strong class="bold-text-form">It's Time To Enhance Your Living</strong></div>
                <div class="b_separator is-short"></div>
                <div class="b_separator"></div>
                <div class="b_separator is-short"></div>
                <div class="main-form-subtitle">Ask For a Totally Free Estimate Today!</div>
                <div class="hero-form-input">
                    <input class="internal-form-input" name="Field1" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Your Name:" data-bvalidator="required">
                </div>
                <div class="hero-form-input">
                    <input class="internal-form-input" name="Field2" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email address:" required="" data-bvalidator="email,required">
                </div>
                <div class="phone-zip-wrap">
                    <div class="hero-form-input zip-field">
                        <input class="internal-form-input" name="Field3" type="tel" value="" maxlength="255" onkeyup="" placeholder="ZIP code">
                    </div>
                    <div class="hero-form-input">
                        <input class="internal-form-input" data-mask="(000) 000-0000" name="Field4" type="tel" value="" maxlength="10" onkeyup="" placeholder="Phone:" data-bvalidator="minlength[10],required">
                    </div>
                </div>
                <div class="submit">
                    <input type="submit" value="Get Your Free Quote" data-wait="Please wait..." class="form-btn">
                </div>
                <div style="list-style: none">
                    <input type="hidden" name="idstamp" value="UZepK7enIuWNt7mINk08q2+A7UXsEo2RHq8VEum1+/g=">
                </div>
                <div class="trust-logos">
                    <div class="b-left-flex">
                        <img src="https://uploads-ssl.webflow.com/5b1e70f3c59e5c55cd84d523/5b29755d0f21cf4ad4b6ce86_energystar.png" width="120">
                    </div>
                    <div class="b-flex-right">
                        <img src="https://uploads-ssl.webflow.com/5b1e70f3c59e5c55cd84d523/5b297a5802c5c63d79c30168_bbb-logo.png" width="120" class="image">
                    </div>
                </div>
            </form>
        </div>
    <?php endif; ?>
    <?php endif; ?>
</div>
<?php if(is_page_template('template/sidebar-page.php') || is_page_template('template/contact-page.php') || is_page_template('template/no-form-page.php')): ?>
<?php else: ?>
<?php if( get_field('hide_form') !== 'Hide' ): ?>
<a href="<?php echo get_page_link(159); ?>" class="btn btn-default cta-internal-btn">Get a Free Estimate Today</a>
<?php endif; ?>
<?php endif; ?>