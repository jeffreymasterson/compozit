<?php
/**
 * User: jmast
 * Date: 8/25/2018
 * Time: 8:35 PM
 */
//Paths
$theme_dir = get_bloginfo('template_directory');
?>
<div class="products-wrapper">
    <h1 class="product-selector-title">How Can We Help You?</h1>
    <div class="b_separator is-short green-version"></div>
    <div class="b_separator green-version"></div>
    <div class="b_separator green-version is-short"></div>
    <div class="products-and-tabs">
        <ul class="tabs-menu nav nav-tabs">
            <?php if(get_field('product_selector','option')): $i = 0;
                while ( have_rows('product_selector', 'option') ) : the_row();
                    if ( have_rows( 'product', 'option' ) ) :
                        while ( have_rows( 'product', 'option' ) ) : the_row();
                            $i++;
                            $icon = get_sub_field( 'product_icon', 'option' );
                            $product_name = get_sub_field( 'product_name', 'option' ); ?>
                            <li>
                                <a data-toggle="tab"  class="products-tabs-links is-product lazyload <?php if($i == 1){ echo 'active'; }?>" href="#product-<?php echo $i; ?>" data-bg="<?php echo $icon; ?>">
                                    <div class="text-block"><?php echo $product_name; ?></div>
                                </a>
                            </li>
                        <?php endwhile;
                    endif;
                endwhile;
            endif; ?>
        </ul>
        <div class="tab-content">
            <?php if(get_field('product_selector','option')): $i = 0;
                while ( have_rows('product_selector', 'option') ) : the_row();
                    if ( have_rows( 'product', 'option' ) ) :
                        while ( have_rows( 'product', 'option' ) ) : the_row();
                            $i++;
                            $product_name = get_sub_field( 'product_name', 'option' );
                            $product_title = get_sub_field( 'product_title', 'option' );
                            $product_subtitle = get_sub_field( 'product_sub_title', 'option' );
                            $product_text = get_sub_field( 'product_text', 'option' );
                            $btn_one_text = get_sub_field( 'button_one_text', 'option' );
                            $btn_one_link = get_sub_field( 'button_one_link', 'option' );
                            $btn_two_text = get_sub_field( 'button_two_text', 'option' );
                            $btn_two_link = get_sub_field( 'button_two_link', 'option' );
                            $bg_image = get_sub_field( 'product_background_image', 'option' ); ?>
                            <div id="product-<?php echo $i; ?>" class="product-tab tab-pane <?php if ($i == 1):?><?php echo'active' ?> <?php endif?>">
                                <div class="product-flex-col">
                                    <h3 class="b-h3-title"><?php echo $product_title; ?></h3>
                                    <div class="b-content-subtitle"><?php echo $product_subtitle; ?></div>
                                    <p class="content-text"><?php echo $product_text; ?></p>
                                    <a href="<?php echo $btn_one_link; ?>" class="btn btn-default tab-btn"><?php echo $btn_one_text; ?></a>
                                    <a href="<?php echo $btn_two_link; ?>" class="btn btn-secondary tab-btn-two"><?php echo $btn_two_text; ?></a>
                                </div>
                                <div class="tabs-background-wrap">
                                    <div class="tabs-background-img lazyload" data-bg="<?php echo $bg_image; ?>"></div>
                                    <div class="tabs-background-gradients"></div>
                                </div>
                            </div>
                        <?php endwhile;
                    endif;
                endwhile;
            endif; ?>
        </div>
    </div>
</div>
