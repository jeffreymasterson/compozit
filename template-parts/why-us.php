<?php

//Paths
$theme_dir = get_bloginfo('template_directory');

$why_title = get_field( 'why_us_title', 'option' );
$why_subtitle = get_field( 'why_us_sub_title', 'option' );
$why_text = get_field( 'why_us_text', 'option' );
$why_image = get_field( 'why_us_image', 'option' );
$why_btn_text = get_field( 'why_us_button_text', 'option' );
$why_btn_link = get_field( 'why_us_button_link', 'option' );

?>
<div class="why-us w-clearfix">
    <h1 class="b-h1-titles">Why Choose Us</h1>
    <div class="b_separator green-version is-short"></div>
    <div class="b_separator green-version"></div>
    <div class="b_separator is-short green-version"></div>
    <div class="why-us-wrap">
        <h3 class="h3-title"><?php echo $why_title; ?></h3>
        <div class="b-content-subtitle is-about-windows"><?php echo $why_subtitle; ?></div>
        <?php echo $why_text; ?>
        <a href="<?php echo $why_btn_link; ?>" class="btn btn-default why-btn"><?php echo $why_btn_text; ?></a></div>
    <div class="why-us-backgorund-img lazyload" data-bg="<?php echo $why_image; ?>"></div>
    <div class="b-tabs-background-gradients is-flip-to-left"></div>
</div>
