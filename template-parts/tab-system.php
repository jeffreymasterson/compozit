<?php
$product_category = get_field( 'service_type' );
?>

<div class="tabs-section">
    <ul class="tabs-menu nav nav-tabs">
        <?php if(get_field('tab_system')): $i = 0;
            while ( have_rows('tab_system') ) : the_row();
                if ( have_rows( 'tab_info') ) :
                    while ( have_rows( 'tab_info') ) : the_row();
                        $i++;
                        $tab_name = get_sub_field( 'tab_name'); ?>
                        <li>
                            <a class="body-tabs <?php if($i == 1){ echo 'active'; }?>" data-toggle="tab" href="#tab-<?php echo $i; ?>"><?php echo $tab_name; ?></a>
                        </li>
                    <?php endwhile;
                endif;
            endwhile;
        endif; ?>
    </ul>
    <div class="tab-content">
        <?php if(get_field('tab_system')): $i = 0;
            while ( have_rows('tab_system') ) : the_row();
                if ( have_rows( 'tab_info') ) :
                    while ( have_rows( 'tab_info') ) : the_row();
                        $i++;
                        $tab_content = get_sub_field( 'tab_content'); ?>
                        <div id="tab-<?php echo $i; ?>" class="tab-pane <?php if($i == 1){ echo 'active'; }?>">
                            <?php echo $tab_content; ?>
                        </div>
                    <?php endwhile;
                endif;
            endwhile;
        endif; ?>
    </div>
</div>