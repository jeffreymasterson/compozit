<div class="sticky-nav">
    <div class="navbar-wrapper">
        <a href="<?php echo home_url( '/' ); ?>" class="logo-sticky">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="162">
        </a>
        <div class="form-wrapper">
            <div class="form-area">
                <form name="form4959" action="https://sociusmarketing.wufoo.com/forms/rje3nou1nk5z5k/#public" method="post" novalidate="" class="sticky-form bvalidate-form">
                    <input class="sticky-form_fields" name="Field1" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Your Name:" data-bvalidator="required">
                    <input class="sticky-form_fields" name="Field2" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email address:" required="" data-bvalidator="email,required">
                    <input class="sticky-form_fields" name="Field4" data-mask="(000) 000-0000" type="text" value="" maxlength="10" onkeyup="" placeholder="Phone:" data-bvalidator="minlength[10],required">
                    <input class="sticky-form_fields" name="Field3" type="text" value="" maxlength="255" onkeyup="" placeholder="ZIP code">
                    <input type="submit" value="Get Quote" data-wait="Please wait..." class="btn btn-default sticky-btn">
                    <div style="list-style: none">
                        <input type="hidden" name="idstamp" value="j1tNiT8RBWjXzwPesc+AtOwjnEJDpDuUjBqCHfcJSFM=">
                    </div>
                </form>
            </div>
        </div>
        <div class="sticky-menu-btn">
            <a class="menu-toggle" href="#mmenu">
                <i class="fa fa-bars"></i>
            </a>
        </div>
    </div>
</div>