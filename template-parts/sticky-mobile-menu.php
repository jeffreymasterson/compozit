<?php

//Paths
$theme_dir = get_bloginfo('template_directory');

?>
<div class="mobile-menu">
    <a href="<?php echo get_page_link(10); ?>" class="one-app window lazyload" data-bg="<?php echo $theme_dir; ?>/images/white-window-icon.png">
        <div>Windows</div>
    </a>
    <a href="<?php echo get_page_link(11); ?>" class="one-app door lazyload" data-bg="<?php echo $theme_dir; ?>/images/white-door-icon.png">
        <div>Doors</div>
    </a>
    <a href="<?php echo get_page_link(12); ?>" class="one-app bathroom lazyload" data-bg="<?php echo $theme_dir; ?>/images/white-bathroom-icon.png">
        <div>Bath</div>
    </a>
    <a href="<?php echo get_page_link(13); ?>" class="one-app siding lazyload" data-bg="<?php echo $theme_dir; ?>/images/white-siding-icon.png">
        <div>Siding</div>
    </a>
</div>