<header class="site-header">
    <div class="b-phone-bar">
        <div class="b-angle-top w-clearfix">
            <div class="b-top-menu_items"><a href="<?php echo get_page_link(154); ?>">Careers</a></div>
            <div class="b-top-menu_items"><a href="<?php echo get_page_link(16); ?>">Contact Us</a></div>
            <div class="b-phonenumber"><a href="tel:<?php echo strip_phone_number('1-800-476-1966'); ?>">1-800-476-1966</a></div>
        </div>
    </div>
    <div class="main-navbar">
        <a class="logo-header" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="196"/>
        </a>
        <nav class="site-navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'd-none d-lg-block' ) ); ?>
        </nav>
        <a class="menu-toggle d-lg-none" href="#mmenu"><i class="fa fa-bars"></i></a>
    </div>
</header><!-- #masthead -->