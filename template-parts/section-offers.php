<?php

//Paths
$theme_dir = get_bloginfo('template_directory');

$title_text = get_field( 'cta_title_text', 'option' );
$cta_text = get_field( 'cta_text', 'option' );
$button_text = get_field( 'cta_button_text', 'option' );
$button_link = get_field( 'cta_button_link', 'option' );
$video_link = get_field( 'cta_video_link', 'option' );


?>
<div class="offer-overflow-wrapper">
    <div class="offers-wrapper lazyload" data-bg="<?php echo $theme_dir; ?>/images/offers-bg.png">
        <div class="offers-main">
            <div class="text-wrap">
                <h1 class="b-h1-titles"><?php echo $title_text; ?><br></h1>
                <div class="standard-paragraph is-offers"><?php echo $cta_text; ?></div>
                <a href="<?php echo $button_link; ?>" class="btn btn-default"><?php echo $button_text; ?></a>
            </div>

            <div class="offers-image lazyload" data-bg="<?php echo $theme_dir; ?>/images/video-bg.jpg">
                <a data-lity href="<?php echo $video_link; ?>"><span class="offers-link-icon lazyload" data-bg="<?php echo $theme_dir; ?>/images/icon-play.png"></span></a>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $(document).scroll(function(){
            if($(this).scrollTop() >= $('.offers-wrapper').offset().top - 400) {
                $(".offers-wrapper").addClass("offers-section-added");
            } else {
            }
        });
    });
</script>