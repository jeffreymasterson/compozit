<?php

//Paths
$theme_dir = get_bloginfo('template_directory');

$top_text = get_field( 'top_text', 'option' );
$middle_text = get_field( 'middle_text', 'option' );
$special_text = get_field( 'special_amount', 'option' );
$bottom_text = get_field( 'bottom_text', 'option' );
$button_text = get_field( 'button_text_sweeps', 'option' );
$button_link = get_field( 'button_link_sweeps', 'option' );
$sweeps_bg = get_field( 'sweepstakes_image', 'option' );

?>
<div class="sweepstakes-content lazyload" data-bg="<?php echo $theme_dir; ?>/images/sweepstakes-bkg.jpg">
    <div class="sweepstakes-flex-right lazyload"></div>
    <div class="sweepstakes-flex-left">
        <div class="is-sweepstakes"><?php echo $top_text; ?></div>
        <div class="separators">
            <div class="b_separator is-short green-version"></div>
            <div class="b_separator green-version"></div>
            <div class="b_separator green-version is-short"></div>
        </div>
        <div class="b-sweepstakes-subtitle"><?php echo $middle_text; ?></div>
        <div class="sweepstakes-30000">$<?php echo $special_text; ?></div>
        <div class="b-sweepstakes-img">
            <div class="b-number">
                <div class="b-up-title-text"><?php echo $bottom_text; ?></div>
            </div>
            <a href="<?php echo $button_link; ?>" class="btn btn-default"><?php echo $button_text; ?></a>
        </div>
    </div>
</div>
<script>
    window.lazySizesConfig = {
        addClasses: true
    };
</script>