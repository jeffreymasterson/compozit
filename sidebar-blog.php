<?php
$search_title = get_field('blog_search_title', 'options');
$categories_title = get_field('blog_categories_title', 'options');
$newsletter_title = get_field('blog_newsletter_title', 'options');

$theme_dir = get_bloginfo('template_directory');
?>
<div class="blog-flex-col inside-sidebar-blog-col">
    <div class="blog-sidebar-section-item-wrap categories-list">
        <div class="blog-sidebar-section-item-title-wrap">
            <h3 class="blog-sidebar-section-item-title">Categories</h3>
        </div>
        <ul>
            <?php wp_list_categories('orderby=name&title_li=&exclude=1'); ?>
        </ul>
    </div>
    <div class="blog-sidebar-section-item-wrap">
        <div class="blog-sidebar-section-item-title-wrap">
            <h3 class="blog-sidebar-section-item-title">Recent Posts</h3>
        </div>
        <ul class="blog-categories-list">
            <?php
            $args = array( 'posts_per_page' => '3' );
            $recent_posts = new WP_Query($args);
            while( $recent_posts->have_posts() ) :
                $recent_posts->the_post() ?>
                <li class="blog-categories-list-item">
                    <a class="blog-categories-list-item-thumb-link" href="<?php echo get_permalink() ?>">
                        <?php if ( has_post_thumbnail() ) : ?>
                            <?php the_post_thumbnail('thumbnail') ?>
                        <?php else:?>
                            <img class="lazyload" data-src="<?php echo $theme_dir; ?>/images/compozit-blog.jpg" src="<?php echo $theme_dir; ?>/images/dummy.png" alt="Compozit blog image">
                        <?php endif ?>
                        <div class="flex-col-image-overlay"></div>
                    </a>
                    <div class="blog-categories-list-item-text-wrap">
                        <a href="<?php echo get_permalink() ?>" class="blog-categories-list-link"><?php the_title() ?></a>
                    </div>
                </li>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); # reset post data so that other queries/loops work ?>
        </ul>
    </div>
    <div class="blog-sidebar-section-item-wrap categories-list">
        <div class="blog-sidebar-section-item-title-wrap">
            <h3 class="blog-sidebar-section-item-title">Archive</h3>
        </div>
        <ul>
            <?php wp_get_archives('type=monthly'); ?>
        </ul>
    </div>
</div>