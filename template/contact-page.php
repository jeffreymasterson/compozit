<?php
/**
 * Template Name: Contact Page
 **/

get_header(); ?>

	<div id="primary" class="content-area default-area">
		<main id="main" class="site-main default-main with-sidebar show-sidebar-mobile">
            <div class="contact-sidebar">
                <div class="side-navigation-column-header">
                    <h4 class="white-text">Contact Us Today!</h4>
                </div>
                <div class="side-navigation-column-navigation-item">
                    <div><strong>Address:</strong><br>5611 Fern Valley Rd,<br>Louisville, KY 40228</div>
                </div>
                <div class="side-navigation-column-navigation-item">
                    <div><strong>Phone:</strong><br><a href="tel:<?php echo strip_phone_number('(502)614-8775'); ?>">(502) 614-8775</a></div>
                    <div><strong>Toll Free:</strong><br><a href="tel:<?php echo strip_phone_number('1-800-476-1966'); ?>">1-800-476-1966</a></div>
                </div>
                <div class="side-navigation-column-navigation-item">
                    <div><strong>Hours:</strong><br> Monday to Friday, 9am to 7pm</div>
                </div>
            </div>
            <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				/*if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif; */

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php
//get_sidebar();
get_footer();
