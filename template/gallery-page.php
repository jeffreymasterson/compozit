<?php
/**
 * Template Name: Gallery Page
 **/

get_header(); ?>

    <div id="primary" class="content-area default-area">
        <main id="main" class="site-main default-main gallery-page">
            <div class="main-content-area" >
                <?php
                while ( have_posts() ) : the_post();
                    get_template_part( 'template-parts/content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    /*if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif; */

                endwhile; // End of the loop.
                ?>
                <?php get_template_part('template-parts/main-gallery');?>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_template_part('template-parts/section-offers');?>
<?php get_template_part('template-parts/sweepstakes');?>

<?php
//get_sidebar();
get_footer();
