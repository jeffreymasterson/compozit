<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package socius_custom
 */

?>


    <?php
    if( is_front_page() ){

    }
    elseif ( is_404() || is_single()  ) {

    }
    ?>

	</div><!-- #content -->

	<?php get_template_part( 'template-parts/content', 'block-footer' ); ?>
	
	<?php get_template_part( 'template-parts/content', 'sticky' ); ?>

	<?php get_template_part( 'template-parts/sticky-mobile-menu' ); ?>

</div><!-- #page -->

<div id="mmenu">   
	<?php //Stitch together menus
		$topmenu = '';
		$primarymenu = '';
		if ( has_nav_menu( 'footer_two' ) ) { $topmenu = wp_nav_menu( array (  'echo' => false, 'theme_location' => 'footer_two', 'items_wrap' => '%3$s' , 'container' => false)); }
		if ( has_nav_menu( 'primary' ) ) {$primarymenu = wp_nav_menu( array (  'echo' => false, 'theme_location' => 'primary', 'items_wrap' => '%3$s' , 'container' => false)); }
		echo '<ul id="mobile-menu">'. $primarymenu . $topmenu  . '</ul>';
	?>
</div>

<?php wp_footer(); ?>

</body>
</html>
