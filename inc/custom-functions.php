<?php
function shortformContact() {
$form_hash = 'uSICbAdz5VvwGyxdxJFOIMLbIAvzrGTQMYNsYrHYpV4=';
$form_action = 'http://sociusmarketing.wufoo.com/forms/r16legy70gnag4e/';
$form_name = 'form4958';
ob_start();
?>

<div class="form-col custom-forms">
    <form name="<?php echo $form_name; ?>" id="<?php echo $form_name; ?>" class="wufoo topLabel page bvalidate-form" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate="" action="<?php echo $form_action; ?>">

        <div class="form-group">
            <label>Name:</label>
            <input name="Field1" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Name *" data-bvalidator="required,minlength[2]" data-bvalidator-msg="Please enter your full name" >
        </div>

        <div class="form-group">
            <label>Phone:</label>
            <input name="Field2" class="form-control" type="tel" value="" maxlength="10" onkeyup="" required="" data-mask="(000) 000-0000" placeholder="Phone *" data-bvalidator="minlength[10],required" data-bvalidator-msg="Please use format (XXX) XXX-XXXX">
        </div>

        <div class="form-group">
            <label>Email:</label>
            <input name="Field3" class="form-control" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email *" required="" data-bvalidator="email,required" data-bvalidator-msg="Please enter a valid email address">
        </div>

        <div class="form-group">
            <label>ZIP Code:</label>
            <input name="Field4" class="form-control" type="tel" value="" maxlength="255" onkeyup="" required="" placeholder="ZIP Code *" data-bvalidator="number,maxlength[5],minlength[5],required" data-bvalidator-msg="Please enter a valid zip code">
        </div>

        <div class="form-group last-form-group">
            <label>Comments:</label>
            <textarea name="Field5" class="form-control" spellcheck="true" rows="10" cols="50" onkeyup="" placeholder="Comments"></textarea>
        </div>
        <div class="buttons form-group">
            <input class="btn btn-default submit-btn" name="saveForm" type="submit" value="Submit">
        </div>
        <div class="form-group" style="list-style: none">
            <input type="hidden" name="idstamp" value="<?php echo $form_hash; ?>">
        </div>
    </form>
</div>
    <?php $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formContact', 'shortformContact' );

function shortformFreeEstimate() {
    $form_hash = 'xlz/sOFby688vZGxHGx0DsTrvYc4iI4kCJkkCUjLIVc=';
    $form_action = 'https://sociusmarketing.wufoo.com/forms/rlekkqq12u1kcb/';
    $form_name = 'form4961';
    ob_start();
    ?>

    <div class="form-col custom-forms">
        <form name="<?php echo $form_name; ?>" id="<?php echo $form_name; ?>" class="wufoo topLabel page bvalidate-form" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate="" action="<?php echo $form_action; ?>">

            <div class="form-group">
                <label>Name:</label>
                <input name="Field1" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Name *" data-bvalidator="required,minlength[2]" data-bvalidator-msg="Please enter your full name" >
            </div>

            <div class="form-group">
                <label>Phone:</label>
                <input name="Field3" class="form-control" type="tel" value="" maxlength="10" onkeyup="" required="" data-mask="(000) 000-0000" placeholder="Phone *" data-bvalidator="minlength[10],required" data-bvalidator-msg="Please use format (XXX) XXX-XXXX">
            </div>

            <div class="form-group">
                <label>Email:</label>
                <input name="Field2" class="form-control" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email *" required="" data-bvalidator="email,required" data-bvalidator-msg="Please enter a valid email address">
            </div>

            <div class="form-group">
                <label>ZIP Code:</label>
                <input name="Field4" class="form-control" type="tel" value="" maxlength="255" onkeyup="" required="" placeholder="ZIP Code *" data-bvalidator="number,maxlength[5],minlength[5],required" data-bvalidator-msg="Please enter a valid zip code">
            </div>

            <div class="form-group last-form-group">
                <label>Comments:</label>
                <textarea name="Field5" class="form-control" spellcheck="true" rows="10" cols="50" onkeyup="" placeholder="Comments"></textarea>
            </div>
            <div class="buttons form-group">
                <input class="btn btn-default submit-btn" name="saveForm" type="submit" value="Submit">
            </div>
            <div class="form-group" style="list-style: none">
                <input type="hidden" name="idstamp" value="<?php echo $form_hash; ?>">
            </div>
        </form>
    </div>
    <?php $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formFreeEstimate', 'shortformFreeEstimate' );

function shortformSweepstakes() {
    $form_hash = '27lAkKTT/EnAW/ra4snlfIWqsAoa+aN2Pn2OziCMu0s=';
    $form_action = 'https://sociusmarketing.wufoo.com/forms/r1b452au03935il/';
    $form_name = 'form4963';
    ob_start();
    ?>

    <div class="form-col custom-forms">
        <form name="<?php echo $form_name; ?>" id="<?php echo $form_name; ?>" class="wufoo topLabel page bvalidate-form" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate="" action="<?php echo $form_action; ?>">
            <div class="form-group">
                <label>First Name:</label>
                <input name="Field1" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Name *" data-bvalidator="required,minlength[2]" data-bvalidator-msg="Please enter your first name" >
            </div>
            <div class="form-group">
                <label>Last Name:</label>
                <input name="Field107" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Name *" data-bvalidator="required,minlength[2]" data-bvalidator-msg="Please enter your last name" >
            </div>
            <div class="form-group">
                <label>Phone:</label>
                <input name="Field3" class="form-control" type="tel" value="" maxlength="10" onkeyup="" required="" data-mask="(000) 000-0000" placeholder="Phone *" data-bvalidator="minlength[10],required" data-bvalidator-msg="Please use format (XXX) XXX-XXXX">
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input name="Field2" class="form-control" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email *" required="" data-bvalidator="email,required" data-bvalidator-msg="Please enter a valid email address">
            </div>
            <div class="form-group">
                <label>Stree Address:</label>
                <input name="Field108" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Address *" data-bvalidator="required">
            </div>
            <div class="form-group">
                <label>City:</label>
                <input name="Field110" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="City *" data-bvalidator="required">
            </div>
            <div class="form-group">
                <label>State:</label>
                <input name="Field111" class="form-control" type="text" value="" maxlength="2" onkeyup="" required="" placeholder="State *" data-bvalidator="required">
            </div>
            <div class="form-group">
                <label>ZIP Code:</label>
                <input name="Field4" class="form-control" type="tel" value="" maxlength="255" onkeyup="" required="" placeholder="ZIP Code *" data-bvalidator="number,maxlength[5],minlength[5],required" data-bvalidator-msg="Please enter a valid zip code">
            </div>
            <div class="form-group">
                <label>How did you hear about our sweepstakes?:</label>
                <textarea name="Field112" class="form-control" spellcheck="true" rows="10" cols="50" onkeyup="" placeholder="Comments"></textarea>
            </div>
            <div class="form-group form-group-flex">
                <label style="width: 100%">Home Improvements You're Most Interested In:</label>
                <div class="checkbox-form">
                    <label class="choice" for="Field6">Windows</label>
                    <input name="Field6" type="checkbox" value="Windows">
                </div>
                <div class="checkbox-form">
                    <label class="choice" for="Field7">Patio/Entry Doors</label>
                    <input name="Field7" type="checkbox" value="Patio/Entry Doors">
                </div>
                <div class="checkbox-form">
                    <label for="Field8">Gutter Protection</label>
                    <input name="Field8" type="checkbox" value="Gutter Protection">
                </div>
                <div class="checkbox-form">
                    <label class="choice" for="Field9">Vinyl Siding</label>
                    <input name="Field9" type="checkbox" value="Vinyl Siding">
                </div>
                <div class="checkbox-form">
                    <label class="choice" for="Field10">Tub/Bath Liner</label>
                    <input name="Field10" type="checkbox" value="Tub/Bath Liner">
                </div>
            </div>
            <div class="form-group form-group-flex last-form-group">
                <label style="width: 100%">Please inform me of specials and promotions:</label>
                <input id="radioDefault_214" name="Field214" type="hidden" value="" />
                <div class="checkbox-form-choice">
                    <label class="choice" for="Field214_0">Yes</label>
                    <input id="Field214_0" name="Field214" type="radio" class="field radio" value="Yes" tabindex="15" onchange="" checked="checked" />
                </div>
                <div class="checkbox-form-choice">
                    <label class="choice" for="Field214_1">No</label>
                    <input id="Field214_1" name="Field214" type="radio" class="field radio" value="No" tabindex="16" onchange=""/>
                </div>
            </div>
            <div class="buttons form-group last-form-group">
                <input class="btn btn-default submit-btn" name="saveForm" type="submit" value="Submit">
            </div>
            <div class="form-group" style="list-style: none">
                <input type="hidden" name="idstamp" value="<?php echo $form_hash; ?>">
            </div>
        </form>
    </div>
    <?php $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formSweepstakes', 'shortformSweepstakes' );

function shortformSchedule() {
    $form_hash = 'FAjAwPdKWKb1NzGvkF7HujQmC7nXgxrXngZeUEKxBZk=';
    $form_action = 'https://sociusmarketing.wufoo.com/forms/rsv2vbr0h0kbmq/';
    $form_name = 'form4962';
    ob_start();
    ?>

    <div class="form-col custom-forms">
        <form name="<?php echo $form_name; ?>" id="<?php echo $form_name; ?>" class="wufoo topLabel page bvalidate-form" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate="" action="<?php echo $form_action; ?>">

            <div class="form-group">
                <label>Name:</label>
                <input name="Field1" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Name *" data-bvalidator="required,minlength[2]" data-bvalidator-msg="Please enter your full name" >
            </div>

            <div class="form-group">
                <label>Phone:</label>
                <input name="Field3" class="form-control" type="tel" value="" maxlength="10" onkeyup="" required="" data-mask="(000) 000-0000" placeholder="Phone *" data-bvalidator="minlength[10],required" data-bvalidator-msg="Please use format (XXX) XXX-XXXX">
            </div>

            <div class="form-group">
                <label>Email:</label>
                <input name="Field2" class="form-control" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email *" required="" data-bvalidator="email,required" data-bvalidator-msg="Please enter a valid email address">
            </div>

            <div class="form-group">
                <label>ZIP Code:</label>
                <input name="Field4" class="form-control" type="tel" value="" maxlength="255" onkeyup="" required="" placeholder="ZIP Code *" data-bvalidator="number,maxlength[5],minlength[5],required" data-bvalidator-msg="Please enter a valid zip code">
            </div>

            <div class="buttons form-group">
                <input class="btn btn-default submit-btn" name="saveForm" type="submit" value="Submit">
            </div>
            <div class="form-group" style="list-style: none">
                <input type="hidden" name="idstamp" value="<?php echo $form_hash; ?>">
            </div>
        </form>
    </div>
    <?php $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formSchedule', 'shortformSchedule' );

function shortformCareers() {
    $form_hash = 'j+XpEiUbh4oBjYBNpzrUD/uEYocAARG5pVuFyfV63co=';
    $form_action = 'https://sociusmarketing.wufoo.com/forms/r1ar14t01g4pfo6/';
    $form_name = 'form4987';
    ob_start();
    ?>

    <div class="form-col custom-forms">
        <form name="<?php echo $form_name; ?>" id="<?php echo $form_name; ?>" class="wufoo topLabel page bvalidate-form" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate="" action="<?php echo $form_action; ?>">

            <div class="form-group">
                <label>First Name:</label>
                <input name="Field1" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Name *" data-bvalidator="required,minlength[2]" data-bvalidator-msg="Please enter your full name" >
            </div>
            <div class="form-group">
                <label>Last Name:</label>
                <input name="Field2" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Last Name *" data-bvalidator="required">
            </div>

            <div class="form-group">
                <label>Home Phone:</label>
                <input name="Field8" class="form-control" type="tel" value="" maxlength="10" onkeyup="" required="" data-mask="(000) 000-0000" placeholder="Home Phone *" data-bvalidator="minlength[10],required" data-bvalidator-msg="Please use format (XXX) XXX-XXXX">
            </div>
            <div class="form-group">
                <label>Cell Phone:</label>
                <input name="Field9" class="form-control" type="tel" value="" maxlength="10" onkeyup="" data-mask="(000) 000-0000" placeholder="Cell Phone">
            </div>

            <div class="form-group">
                <label>Email:</label>
                <input name="Field12" class="form-control" type="email" spellcheck="false" value="" maxlength="255" placeholder="Email *" required="" data-bvalidator="email,required" data-bvalidator-msg="Please enter a valid email address">
            </div>

            <div class="form-group">
                <label>Stree Address:</label>
                <input name="Field4" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="Address *" data-bvalidator="required">
            </div>
            <div class="form-group">
                <label>City:</label>
                <input name="Field5" class="form-control" type="text" value="" maxlength="255" onkeyup="" required="" placeholder="City *" data-bvalidator="required">
            </div>
            <div class="form-group">
                <label>State:</label>
                <input name="Field6" class="form-control" type="text" value="" maxlength="2" onkeyup="" required="" placeholder="State *" data-bvalidator="required">
            </div>
            <div class="form-group">
                <label>ZIP Code:</label>
                <input name="Field7" class="form-control" type="tel" value="" maxlength="255" onkeyup="" required="" placeholder="ZIP Code *" data-bvalidator="number,maxlength[5],minlength[5],required" data-bvalidator-msg="Please enter a valid zip code">
            </div>

            <div class="form-group">
                <label>Attach File (250Mb Limit):</label>
                <input id="Field10" name="Field10" type="file" class="form-control" size="12" data-file-max-size="10" />
            </div>
            <div class="buttons form-group" style="clear:both;">
                <input class="btn btn-default submit-btn" name="saveForm" type="submit" value="Submit">
            </div>
            <div class="form-group" style="display: none">
                <input type="hidden" name="idstamp" value="<?php echo $form_hash; ?>">
            </div>
        </form>
    </div>
    <?php $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formCareers', 'shortformCareers' );


function strip_phone_number($phone = ''){
	return preg_replace('/\D+/', '', $phone);
}
?>