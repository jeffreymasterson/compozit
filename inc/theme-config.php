<?php
/**
 * Enqueue scripts and styles.
 */
function socius_custom_scripts() {
	if ( ! is_admin() ) {
        // deregister the original version of jQuery
        wp_deregister_script('jquery');
        // register it again, this time with no file path
        wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', FALSE, '1.12.4');
        // add it back into the queue
        wp_enqueue_script('jquery');
    }

    //Fonts
	wp_enqueue_style( 'googlefonts', '//fonts.googleapis.com/css?family=Open+Sans:300,400,400i,700|Montserrat:300,400,600,700,800' );
	wp_enqueue_style( 'fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');


	//CSS minifying in gulp task
		//wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );
		//wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/css/slick.css');
		//wp_enqueue_style( 'mmenu-css', '//cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.7.8/css/jquery.mmenu.all.css');

	//JS minifying in gulp task
		//wp_enqueue_script( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array( 'jquery' ), '1.0', true );
		//wp_enqueue_script( 'slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js', array( 'jquery' ), '1.0', true );
		//wp_enqueue_script( 'lazyload', '//cdn.jsdelivr.net/jquery.lazyload/1.9.3/jquery.lazyload.min.js', array( 'jquery' ), '1.0', true );

		//wp_enqueue_script( 'bvalidator', get_template_directory_uri() . '/js/jquery.bvalidator-yc.js', array( 'jquery' ), '1.0', true );
		//wp_enqueue_script( 'mmenu', '//cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.7.8/js/jquery.mmenu.all.min.js', array( 'jquery' ), '1.0', true );
		//wp_enqueue_script( 'cookie', '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js', array( 'jquery' ), '1.0', true );
		//wp_enqueue_script( 'placeholders', '//cdn.jsdelivr.net/placeholders/3.0.2/placeholders.min.js', array( 'jquery' ), '1.0', true );
		//wp_enqueue_script( 'mask-js', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js', array( 'jquery' ), '1.0', true );
		//wp_enqueue_script( 'library-js', get_template_directory_uri() . '/js/scripts.min.js', array('jquery'), '', true );

	//Theme Files
	wp_enqueue_style( 'vendor-css', get_template_directory_uri() . '/css/vendor.min.css');
	wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/js/vendor.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/js/main.min.js', array('jquery'), '', true );
	wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style( 'lity-css', '//cdnjs.cloudflare.com/ajax/libs/lity/2.3.0/lity.min.css');
    wp_enqueue_script( 'lity-js', '//cdnjs.cloudflare.com/ajax/libs/lity/2.3.0/lity.min.js', array( 'jquery' ), '1.0', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'socius_custom_scripts' );

/**
 * Defer JS when possible
 * Not necessary when gulp has compiled vendor scripts
 */
function add_defer_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_defer = array('bvalidator', 'mmenu', 'cookie', 'placeholders', 'mask-js', 'library-js');

   foreach($scripts_to_defer as $defer_script) {
      if ($defer_script === $handle) {
         return str_replace(' src', ' defer="defer" src', $tag);
      }
   }
   return $tag;
}
//add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

/*
 * Add larger size for use in theme and featured images
 */ 
add_image_size( 'extra_large', 1920, 0, false );

/**
 * Changes the default Wordpress logo to the client logo. Make sure you change the background size, width, and height depending on the logo size.
 */
function my_login_head() {
    echo "
    <style>
    body {
    background: url('".get_bloginfo('template_url')."/images/login-bg.png') no-repeat center center;
    }
    body.login #login h1 a {
        background: url('".get_bloginfo('template_url')."/images/logo.png') no-repeat scroll center top transparent;
        width:236px;
        height: 104px;
        background-size: 236px 90px;
        margin: 0 auto;
    }
    body.login #login #nav a {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
    }
    body.login #login #backtoblog a {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
        
    }
    #loginform {
        box-shadow: 0 6px 10px rgba(0, 0, 0, 0.58);
        border-radius: 5px;
        background: rgba(230, 230, 230, 0.90);
    }
    .login label {
        color: #000000;
        font-size: 14px;
    }
    .wp-core-ui .button-primary {
        background: #018848;
        border-color: #018848 #018848 #018848;
        box-shadow: 0 1px 0 #018848;
        color: #fff;
        text-decoration: none;
        text-shadow: 0 -1px 1px #018848, 1px 0 1px #018848, 0 1px 1px #018848, -1px 0 1px #018848;
    }
    .wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover {
        background: #025c31;
        border-color: #018848;
        color: #fff;
    }
    #loginform {
    -webkit-animation: fadein 2.5s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 2.5s; /* Firefox < 16 */
        -ms-animation: fadein 2.5s; /* Internet Explorer */
         -o-animation: fadein 2.5s; /* Opera < 12.1 */
            animation: fadein 2.5s;
    }
    
    @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
    }
    
    /* Firefox < 16 */
    @-moz-keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }
    
    /* Safari, Chrome and Opera > 12.1 */
    @-webkit-keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }
    
    /* Internet Explorer */
    @-ms-keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }
    
    /* Opera < 12.1 */
    @-o-keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }
    </style>
    ";
}
add_action("login_head", "my_login_head");
remove_action('wp_head', 'wp_generator'); 


/**
 * Register widget area.
 */
function socius_custom_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'socius_custom' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'socius_custom' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'socius_custom_widgets_init' );


/**
 * ACF JSON
 */
add_filter('acf/settings/save_json', function() {
	return get_template_directory() . '/acf-json';
});
add_filter('acf/settings/load_json', function($paths) {
	$paths = array(get_template_directory() . '/acf-json');
	return $paths;
});

/**
 * Add ACF Options page
 */
if( function_exists('acf_add_options_page') ) {
  
	acf_add_options_page(array(
		'page_title'  => 'Theme General Settings',
		'menu_title'  => 'Theme Options',
		'menu_slug'   => 'theme-general-settings',
		'capability'  => 'edit_posts',
		'redirect'    => false
	));

    // Add subpage
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Homepage',
        'menu_title'	=> 'Homepage',
        'parent_slug'	=> 'theme-general-settings',
    ));
  
}

// Limit post revisions
function sm_set_revision_size( $num, $post ) { 
    return 4;  //here allowed revisions are up to 5. 
}
add_filter( 'wp_revisions_to_keep', 'sm_set_revision_size', 10, 2 );