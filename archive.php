<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package newpro
 */

get_header(); ?>

    <div id="primary" class="content-area blog-content">
        <main id="main" class="site-main">
            <div class="container w-container">
                <div class="flex-row">
                    <div class="blog-flex-col flex-col">
                        <?php
                        if ( have_posts() ) : ?>

                            <header class="page-header">
                                <?php
                                the_archive_title( '<h1 class="page-title">', '</h1>' );
                                the_archive_description( '<div class="archive-description">', '</div>' );
                                ?>
                            </header><!-- .page-header -->

                            <?php
                            /* Start the Loop */
                            while ( have_posts() ) : the_post();

                                /*
                                 * Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'template-parts/content', 'blog' );

                            endwhile;

                            the_posts_navigation();

                        else :

                            get_template_part( 'template-parts/content', 'none' );

                        endif; ?>
                    </div>
                    <?php get_sidebar('blog');?>
                </div>
            </div>
            <div class="flex-row">
                <div class="page-navigation-blog">
                    <?php wp_pagenavi(); ?>
                </div>
            </div>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
