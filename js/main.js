var options = {
	classNamePrefix: 'bvalidator_',
	position: {x:'left', y:'top'},
	validClass: true,
	validateOn: 'keyup'
}

function display_sticky() {
    var pos = jQuery(window).scrollTop() || jQuery('html').scrollTop();
    if(pos > 250) {
        jQuery('.sticky-nav').addClass('sticky');
    } else {
        jQuery('.sticky-nav').removeClass('sticky');
    }
}

(function($) {
  var uniqueCntr = 0;
  $.fn.scrolled = function (waitTime, fn) {
      if (typeof waitTime === "function") {
          fn = waitTime;
          waitTime = 250;
      }
      var tag = "scrollTimer" + uniqueCntr++;
      this.scroll(function () {
          var self = $(this);
          var timer = self.data(tag);
          if (timer) {
              clearTimeout(timer);
          }
          timer = setTimeout(function () {
              self.removeData(tag);
              fn.call(self[0]);
          }, waitTime);
          self.data(tag, timer);
      });
  }
})(jQuery);

jQuery(window).scrolled(function() {
  display_sticky();
});

jQuery(document).ready(function() {
	jQuery('.bvalidate-form').bValidator(options);
  jQuery('.phone_us').mask('(000) 000-0000'); 
  //Customize mmenu settings as necessary
	jQuery("#mmenu").mmenu({
		    "navbar": {
		        "title": "",
		    },
			"offCanvas" : {
		        "position" : "right",
		        "zposition" : "front"
		    },
		    classNames: {
	            fixedElements: {
	               fixed: "header"
            }
		}
    });
	jQuery('img').addClass('img-fluid');
  jQuery('a[href*="tel"]').on('click', function() {
    gtag( 'event', 'click', {'event_category' : 'Call', 'event_action' : 'Click', 'event_label' : 'Click to Call'});
  });

  //Allow dummy parent links to click into their child menu in mmenu
  jQuery( "#mmenu li.dummy a.mm-next" ).each( function( index ) {
      var mobileurl = jQuery( this ).attr('href');
      jQuery( this ).next('a').attr('href',mobileurl);
  });

}); //End doc.ready
jQuery(document).ready(function() {

    jQuery('.products-and-tabs').viewportChecker({
        classToAdd: 'bring-selector-up',
        offset: -300
    });

}); //End doc.ready


//Use the following functions if you need to trigger JS at different screen sizes
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}

function sample_function() {
    var vpWidth = viewport().width; //This should match media query
    if (vpWidth < 768) {
      //Do something
    } else {
      //Do something else
    }
}
  jQuery(window).resize(function() {
      sample_function();
  });
  jQuery(document).ready(function() {
      sample_function();
  });